

let target = ['npm'];

module.exports = {
    target,
    tpl: [
        'README.md',
        'LICENSE',
        'package.json',
        'version.js'
    ]
};